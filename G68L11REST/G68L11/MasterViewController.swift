//
//  MasterViewController.swift
//  G68L11
//
//  Created by Ivan Vasilevich on 2/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
// https://parseplatform.org/

import UIKit

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var objects = [Car]()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.

		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		reloadData()
	}
	
	// https://parseplatform.org/
	private func reloadData() {
		sendRequest()
	}

	// MARK: - Table View

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row]
		cell.textLabel!.text = object.name
		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    objects.remove(at: indexPath.row)
		    tableView.deleteRows(at: [indexPath], with: .fade)
		} else if editingStyle == .insert {
		    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}
	
	func sendRequest() {
		
		let sessionConfig = URLSessionConfiguration.default

		/* Create session, and optionally set a URLSessionDelegate. */
		let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

		/* Create the Request:
		   Drinks (GET https://parseapi.back4app.com/classes/Car)
		 */

		guard var URL = URL(string: "https://parseapi.back4app.com/classes/Car") else {return}
		var request = URLRequest(url: URL)
		request.httpMethod = "GET"

		// Headers

		request.addValue("AcX1Wh5zq9MNBDl8tvZKKux2ByZRuZk3qS3flSIn", forHTTPHeaderField: "X-Parse-REST-API-Key")
		request.addValue("m66HpZYmjiQoOe4I46UmVXw4OJYTgp9QtffuVU7E", forHTTPHeaderField: "X-Parse-Application-Id")

		URLSession.fetchData(type: CarsResponse.self, request: request) { (response, error) in
			guard let response = response else { return }
			self.objects = response.results
			self.tableView.reloadData()
		}
	}

}

class CarsResponse: Decodable {
	var results: [Car]
}

class Car: Decodable {
	let name: String
}
