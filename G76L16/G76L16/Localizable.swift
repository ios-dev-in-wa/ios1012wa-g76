//
//  Localizable.swift
//  G76L16
//
//  Created by Ivan Vasilevich on 13.02.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import Foundation

struct Localizable {
	static let cancel = NSLocalizedString("Cancel", comment: "cancel button title on alert")
}
