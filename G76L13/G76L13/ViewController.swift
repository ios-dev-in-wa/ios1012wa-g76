//
//  ViewController.swift
//  G76L13
//
//  Created by Ivan Vasilevich on 04.02.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		view.backgroundColor = .red
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		displayAlert()
	}
	
	func displayAlert() {
		let alert = UIAlertController(title: "Title", message: "My message hello!", preferredStyle: .alert)
		
		alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
		alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (_) in
			self.playSound()
		}))
		if UIDevice.current.userInterfaceIdiom == .pad {
			alert.modalPresentationStyle = .popover

			if let popoverController = alert.popoverPresentationController {
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			}
		}
		present(alert, animated: true, completion: nil)
	}
	
	var player: AVAudioPlayer!
	
	func playSound() {
		let url = Bundle.main.url(forResource: "pew-pew-lei", withExtension: "caf")!
		do {
			try AVAudioSession.sharedInstance().setCategory(.ambient)
			player = try AVAudioPlayer(contentsOf: url, fileTypeHint: nil)
		}
		catch {
			print(error)
			return
		}
		player.play()
		
	}


}

