//
//  TableViewController.swift
//  G76L13
//
//  Created by Ivan Vasilevich on 04.02.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
	
	
	var items: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
		let customCellnib = UINib(nibName: "TableViewCell", bundle: nil)
		tableView.register(customCellnib, forCellReuseIdentifier: "CustomTableViewCell")

		items =
"""
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
"""
	.components(separatedBy: " ")

    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
		return items.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as! CustomTableViewCell

        // Configure the cell...
		let itemToDisplay = items[indexPath.row]
		cell.leftLabel.text = itemToDisplay
		cell.rightLabel.text = String(itemToDisplay.reversed())
        return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "showIntro", sender: nil)
	}
	
	override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		let greenVC = UIStoryboard(name: "Intro", bundle: nil).instantiateViewController(identifier: "GreenVC")
		greenVC.title = "privet"
		present(greenVC, animated: true, completion: nil)
	}

}
