//
//  CustomTableViewCell.swift
//  G76L13
//
//  Created by Ivan Vasilevich on 04.02.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
	
	@IBOutlet weak var leftLabel: UILabel!
	@IBOutlet weak var rightLabel: UILabel!
	@IBOutlet weak var decideSwitch: UISwitch!
    
}
