//
//  ViewController.swift
//  G76L12
//
//  Created by Ivan Vasilevich on 30.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var tableView: UITableView!
	
	private var objects: [String] = ["car0", "car1", "car2"]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		setupView()
	}
	
	private func setupView() {
		title = "SEMA 2020"
		tableView.dataSource = self
//		tableView.delegate = self
	}


}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "carCell", for: indexPath)
		let objectToDisplay = objects[indexPath.row]
		cell.textLabel?.text = objectToDisplay
		return cell
	}
	
	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let objectToDisplay = objects[indexPath.row]
		let objectImage = UIImage(named: objectToDisplay)
		imageView.image = objectImage
		performSegue(withIdentifier: "showDetailItem", sender: objectToDisplay)
	}
}

