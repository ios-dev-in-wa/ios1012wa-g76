
import Foundation

struct Place: Decodable {
    let name: String
    let desc: String
    let url: String
    
    static func fetchPlaces(completion: (([Self]?, Error?) -> Void)?) {
        URLSession.fetchData(type: [Place].self, link: "https://api.myjson.com/bins/16w6h0", completion: completion)
    }
}
