//
//  ViewController.swift
//  G78L10
//
//  Created by Ivan Vasilevich on 23.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
	// MARK: - IBOutlet's
	
	@IBOutlet weak var nameTextField: UITextField!
	
	private var myClosure: SimpleClosure?
	
	// MARK: - VC Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		nameTextField.delegate = self
		
		let numbers = [3, 6, 1, 9]
		
		myClosure = { [weak self] in
			guard let self = self else {
				return
			}
			self.nameTextField.text = "opa"
			print(numbers)
		}
		
		let sortedNumbers = numbers.sorted { (elA, elB) -> Bool in
			return elA > elB
		}
		
		let c1 = Candy(sugar: 0, color: .red)
		c1.makeFoo {
			print("pew pew pew!")
		}
		
	}
	
	// MARK: - UITextFieldDelegate
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return false
	}

	// MARK: - IBAction's
	
	@IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
		print(sender.view!.frame.origin.y)
	}
}

extension ViewController: MakeSound {
	func foo() {
		print("ViewController")
	}
}

typealias SimpleClosure = () -> Void

struct Candy: MakeSound {
	let sugar: Double
	let color: UIColor

	func foo() {
		 print("candy crash")
	}
	
	func makeFoo(block: SimpleClosure) {
		print("before block")
		block()
		print("after block")
	}
}

protocol MakeSound {
	func foo()
}

