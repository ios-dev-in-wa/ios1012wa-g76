//
//  ViewController.swift
//  G76L9
//
//  Created by Ivan Vasilevich on 21.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var finderView: FaceView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		guard let myView = view else {
			print("! myView")
			return
		}
		guard let label = myView.subviews.first?.subviews.first else {
			print("! label")
//			fatalError("message")
			return
		}
		myView.subviews.first?.center = view.center
		label.backgroundColor = .blue
		
		
		if let realView = view {
			if let firstSubview = realView.subviews.first {
				firstSubview.backgroundColor = .red
			}
		}
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		finderView.happyLevel = 20
		finderView.changeHappyFace(withHappLevel: -10, andColor: .red)
	}


}

