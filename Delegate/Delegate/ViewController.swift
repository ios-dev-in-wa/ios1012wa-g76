//
//  ViewController.swift
//  Delegate
//
//  Created by Ivan Vasilevich on 24.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
	@IBOutlet weak var progressView: UIProgressView!
	@IBOutlet weak var textView: UITextView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		progressView.progress = Float (scrollView.contentOffset.y /
		(scrollView.contentSize.height - scrollView.frame.size.height))
		
	}


}

