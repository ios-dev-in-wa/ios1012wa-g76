//
//  ViewController.swift
//  G76L11
//
//  Created by Ivan Vasilevich on 28.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	let myBar = Bar()
	let box = UIView(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		setupBox()
		workWithBar()
	}
	
	private func workWithBar() {
			myBar.foo()
		//		myBar.vc = self
				weak var secondBar = Bar()
				myBar.completion = { [weak self] () -> Void in // unowned/weak
					guard let self = self else {
						return
					}
					self.recolorView()
					secondBar?.foo()
				}
				myBar.foo()
	}
	
	private func setupBox() {
		view.addSubview(box)
		box.backgroundColor = .red
		box.center = view.center
	}
	
	private func recolorView() {
		view.backgroundColor = .green
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let firstTouch = touches.randomElement()!
		let pos = firstTouch.location(in: view)
//		UIView.animate(withDuration: 0.3) {
//			self.box.center = pos
//			self.box.alpha = self.box.alpha == 0 ? 1 : 0
//		}
//		UIView.animate(withDuration: 1, animations: {
//			self.box.center = pos
//			self.box.alpha = self.box.alpha == 0 ? 1 : 0
//		}) { (completed) in
//			if completed {
//				self.dismiss(animated: true, completion: nil)
//			}
//		}
		
		UIView.animate(withDuration: 1, delay: 1, options: [.allowUserInteraction, .repeat, .autoreverse], animations: {
			self.box.center = pos
			self.box.alpha = self.box.alpha == 0 ? 1 : 0
		}, completion: nil)
//		print("start present")
//		present(UIViewController(), animated: true) {
//			print("presented")
//		}
	}

	deinit {
		print("\(self.description) deinited")
	}

}

class Bar {
	
	weak var vc: UIViewController?
	
	var completion: (() -> Void)?
	
	func foo() {
		print("Bar make foo")
		completion?()
	}
	
	deinit {
		completion?()
		print("Bar deinited")
	}
}

