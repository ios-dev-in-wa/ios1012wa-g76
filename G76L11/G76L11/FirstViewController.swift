//
//  FirstViewController.swift
//  G76L11
//
//  Created by Ivan Vasilevich on 28.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
	
	@IBOutlet weak var counterLabel: UILabel!
	private var count = 0 {
		didSet {
			updateUI()
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
	
	private func updateUI() {
		let b1 = self.view.viewWithTag(1)
		let b2 = self.view.viewWithTag(-1)
		let buttons = ([b1, b2] as! [UIButton])
		buttons.forEach { (button) in
			button.isEnabled = false
		}
		UIView.transition(with: counterLabel, duration: 1.33, options: [.transitionCurlUp], animations: {
			self.counterLabel.text = self.count.description
		}) { (_) in
			buttons.forEach { (button) in
				button.isEnabled = true
			}
		}
	}
    
	@IBAction func turnPage(_ sender: UIButton) {
		count += sender.tag
	}
}
