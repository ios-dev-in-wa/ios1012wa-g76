//
//  GreenViewController.swift
//  G76L7
//
//  Created by Ivan Vasilevich on 14.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class GreenViewController: UIViewController {

	var stringTitle = "Demo"
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		navigationItem.title = stringTitle
		(navigationController?.viewControllers.first as? ViewController)?.myLabel.text = "azazas"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
