//
//  ViewController.swift
//  G76L7
//
//  Created by Ivan Vasilevich on 14.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class CoffeMachine {
	var sugar: Int!// = 0
}

class ViewController: UIViewController {
	
	@IBOutlet weak var myLabel: UILabel!
	@IBOutlet weak var missedCallsSegmentControll: UISegmentedControl!
	
	override func awakeFromNib() { //xib // nib
		super.awakeFromNib()
		log()
		print("-------------1-------------------")
		print("missedCallsSegmentControll == nil", missedCallsSegmentControll == nil)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		log()
		print("-------------2-------------------")
		print("missedCallsSegmentControll == nil", missedCallsSegmentControll == nil)
		print("init ViewController")
		// Do any additional setup after loading the view.
		let machine = CoffeMachine()
		machine.sugar = 20
		myLabel.text = machine.sugar.description
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		log()
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		log()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		log()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		log()
		//		tabBarItem.badgeValue = ""
		navigationController?.tabBarItem.badgeValue = nil
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		log()
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		log()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
		log()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let greenVc = segue.destination as? GreenViewController {
			greenVc.stringTitle = "NE DEMO"
		}
	}
	
	@IBAction func segmentCghanged(_ sender: UISegmentedControl) {
		let segueIds = ["showTable",
						"showGreen",
						"showRed"]
		let selectedId = segueIds[sender.selectedSegmentIndex]
		performSegue(withIdentifier: selectedId, sender: nil)
		
	}
}

