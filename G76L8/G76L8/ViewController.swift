//
//  ViewController.swift
//  G76L8
//
//  Created by Ivan Vasilevich on 16.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet private weak var inputATextField: UITextField!
	@IBOutlet private weak var resultLabel: UILabel!
	@IBOutlet private weak var inputBTextView: UITextView!
	
	private var sum = 0
	
	private let kResult = "kResult"
	private let kHistory = "kHistory"
	
	// Computed property
	private var history: String {
		UserDefaults.standard.string(forKey: kHistory) ?? ""
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		resultLabel.text = UserDefaults.standard.string(forKey: kResult)
		inputBTextView.text = history
		
		Test().run()
	}
	
	private func changeTextViewFontSize(to size: CGFloat) {
		let newFont =  inputBTextView.font?.withSize(size)
		inputBTextView.font = newFont
	}
	
	private func updateHistory(with result: String) {
		var history = self.history
		history += "\n\(result)"
		UserDefaults.standard.set(history, forKey: kHistory)
	}
	
	@IBAction func calcButtonPressed() {
		let aValue = Int(inputATextField.text!) ?? 0
		let bValue = Int(inputBTextView.text!) ?? 0
		sum = aValue + bValue
		let stringResult = "\(aValue) + \(bValue) = \(sum)"
		updateHistory(with: stringResult)
		UserDefaults.standard.set(stringResult, forKey: kResult)
		resultLabel.text = stringResult
		changeTextViewFontSize(to: CGFloat(sum))
	}
	
}

