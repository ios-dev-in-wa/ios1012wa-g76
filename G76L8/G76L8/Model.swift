//
//  Model.swift
//  G76L8
//
//  Created by Ivan Vasilevich on 16.01.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import Foundation

class Test {
	func run() {
		
		let carClassObj = CarClass()
		carClassObj.speed = 100
		
		cahngeCarSpeedClass(car: carClassObj)
		print("carClassObj.speed", carClassObj.speed)
		// Refference
		var carClassObj2 = carClassObj
		carClassObj2.speed = -10
		print("carClassObj2.speed", carClassObj2.speed)
		print("carClassObj.speed", carClassObj.speed)
		
		//		carClassObj2 = CarClass()
		
		var carStructObj = CarStruct(speed: 0)
		carStructObj.speed = 100
		print(carStructObj)
		// Copy
		var carStructObj2 = carStructObj
		
		
		let myPos = Point3d(x: 20, y: 18, z: -5)
		myPos.maxPointVal()
		
		cahngeCarSpeedEnum(car: .fastCar)
	}
	
	func cahngeCarSpeedClass(car: CarClass) {
		car.speed += 20
	}
	
	func cahngeCarSpeedStruct(car: CarStruct) {
		//		car.speed += 20
	}
	
	func cahngeCarSpeedEnum(car: CarEnum) {
		//		car.speed += 20
	}
}

class CarClass {
	var speed: Int = 0
}

struct CarStruct {
	var speed: Int
}

struct Point3d {
	var x: Int
	var y: Int
	var z: Int
}

extension Point3d {
	func maxPointVal() -> Int {
		return max(max(self.x, self.y), z)
	}
}

enum CarEnum {
	case slowCar
	case fastCar
	
	var maxSpeed: Int {
		switch self {
		case .slowCar:
			return 20
		case .fastCar:
			return 100
		}
	}

}

extension CarEnum {
}

