//
//  ViewController.swift
//  G76L14
//
//  Created by Ivan Vasilevich on 06.02.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var counterLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		Basket.instance.items.append("item 1")
		counterLabel.layer.cornerRadius = min(counterLabel.frame.height, counterLabel.frame.width) / 2
		NotificationCenter.default.addObserver(self, selector: #selector(updateUI(notification:)), name: Basket.basketItemsDidChangeNotification, object: Basket.instance)
	}
	
	@objc func updateUI(notification: Notification) {
		if let userInfo = notification.userInfo {
			print("userInfo:", userInfo)
		}
		counterLabel.text = Basket.instance.items.count.description
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		view.endEditing(true)
		Basket.instance.items.append(Int.random(in: 0...45).description)
	}
	
	@IBAction func runLoop() {
		let upLimit = Int.max/200000000000000
		
		kBgq.async {
			for i in 0..<upLimit {
				print( "\(i) \t\t: \(upLimit)")
			}
			print("FINISH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
			kMainQueue.async {
				self.view.backgroundColor = .red
			}
		}
		
		
	}
	

}

