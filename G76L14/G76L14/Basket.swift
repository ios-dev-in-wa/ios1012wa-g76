//
//  Basket.swift
//  G76L14
//
//  Created by Ivan Vasilevich on 06.02.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import Foundation

class Basket {
	
	static let basketItemsDidChangeNotification = NSNotification.Name(rawValue: "basketItemsDidChangeNotification")
	static let instance = Basket()
	
	var items: [String] = [] {
		didSet {
			NotificationCenter.default.post(name: Basket.basketItemsDidChangeNotification, object: self, userInfo: ["car": "Opel 1.3"])
		}
	}
}
